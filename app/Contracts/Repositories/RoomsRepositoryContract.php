<?php
namespace App\Contracts\Repositories;

interface RoomsRepositoryContract
{
    public function all();

    public function getById($id);

    public function save($model);
}

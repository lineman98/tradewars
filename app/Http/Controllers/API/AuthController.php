<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Validator;
use Auth;

class AuthController extends Controller
{
    
    public function login(Request $request) {
		$validation = Validator::make($request->all(), [
			'email' => 'required|string|email|max:100|',
			'password' => 'required|string|min:8'
		]);

		if($validation->fails()) {
			return response()->json($validation->errors(), 401);
		}
		
		if(!Auth::once($request->only('email', 'password'))) {
			return response()->json(['password' => __('auth.failed')], 401);
		}

		$token = $request->user()->createToken(date('Y-m-d H:i:s'));

		return response()->json([
			'user' => $request->user(),
			'token' => $token->plainTextToken
		], 200);
	}

	public function register(Request $request) {
		$credentials = $request->all();
		
		$validation = Validator::make($credentials, [
			'email' => 'required|string|email|max:100|unique:users',
			'name' => 'required|string|max:100',
			//'surname' => 'string|min:5|max:100',
			'password' => 'required|string|min:8'
		]);

		if ($validation->fails()) {
			return response()->json($validation->errors(), 401);
		}


		$credentials['password'] = Hash::make($credentials['password']);
		
		$user = User::create($credentials);

		$token = $user->createToken(date('Y-m-d H:i:s'));

		return response()->json([
			'user' => $user,
			'token' => $token->plainTextToken
		], 200);
	} 
}

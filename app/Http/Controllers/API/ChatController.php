<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\ChatRepository;
use App\ChatMessage;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;

class ChatController extends Controller
{
    public function __construct(ChatRepository $messages)
    {
        $this->messages = $messages;
	}
	
	public function history(Request $request, $room, $before = 0)
	{
		$user = $request->user();

		/* TODO: Assertinations for room */

		return $this->messages->getLastSended($room, $before);
	}

	public function post(Request $request, $room)
	{
		$data = $request->all();
		$user = $request->user();

		$validation = Validator::make($request->all(), [
			'body' => 'required|string|max:200|'
		]);

		if($validation->fails()) {
			return response()->json($validation->errors(), 401);
		}

		/* TODO: Assertinations for room, not deleted, floodgate */

		$message = $this->messages->create([
			'room_id' => $room,
			'user_id' => $user,
			'body' => $data['body'],
			'created_at' => Carbon::now()
		]);

		/* TODO: Alert sockets about new message */

		return $message;
	}

	public function edit(Request $request, $room, $message)
	{
		$data = $request->all();
		$user = $request->user();

		$validation = Validator::make($request->all(), [
			'body' => 'required|string|max:200|'
		]);

		if($validation->fails()) {
			return response()->json($validation->errors(), 401);
		}

		/* TODO: Assertions for room, user, not deleted */

		$message = $this->messages->findOrFail($room, $message);
		$message->body = $data['body'];
		$message->updated_at = Carbon::now();

		$message->save();

		/* TODO: Alert sockets about message edited */

		return $message;
	}

	public function delete(Request $request, $room, $message)
	{
		/* TODO: Assertions for room, user, not deleted */
	
		$message = $this->messages->findOrFail($room, $message);
		$message->deleted_at = Carbon::now();

		$message->save();

		/* TODO: Alert sockets about message deleted */

		return $message;
	}
}
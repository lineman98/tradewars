<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use phpcent\Client as Centrifugo;

class HomeController extends Controller
{
    protected $centrifugo;
    public function __construct(Centrifugo $centrifugo)
    {
        $this->centrifugo = $centrifugo;
    }

    public function index(Request $request)
    {
        return view('profile.home');/*, [
            'user' => $request->user(),
            'centrifugoToken' => $this->centrifugo->generateConnectionToken($request->user()->id)
        ]);*/
    }
}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Contracts\Repositories\RoomsRepositoryContract;

class TestController extends Controller
{
    public function __construct(RoomsRepositoryContract $roomsRepository)
    {
        $this->roomsRepository = $roomsRepository;
    }

    public function index()
    {
        dd($this->roomsRepository->all());
    }
}

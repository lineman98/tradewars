<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CentrifugoBroadcaster\CentrifugoBroadcaster;
use Illuminate\Broadcasting\BroadcastManager;
use Illuminate\Foundation\Application;
use phpcent\Client;
use Illuminate\Support\Facades\Broadcast;

class CentrifugoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $config = config('broadcasting.connections.centrifugo');
        $instance = new \phpcent\Client($config['url']);
        $instance->setApiKey($config['api_key']);
        $instance->setSecret($config['secret']);

        $this->app->instance('phpcent\Client', $instance);
        $this->app->alias('phpcent\Client', 'Centrifugo');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(BroadcastManager $broadcastManager)
    {
        $broadcastManager->extend('centrifugo', function (Application $app, array $config) {
            return new CentrifugoBroadcaster($app->make('Centrifugo'));
        });

        Broadcast::routes();

        require base_path('routes/channels.php');
    }
}

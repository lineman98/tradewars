<?php

namespace App\Repositories;

use App\ChatMessage;

class ChatRepository
{
    public function query()
    {
        return ChatMessage::query();
	}
	
    public function queryVisible($room)
    {
        return $this->query()->where('room_id', $room)->whereNull('deleted_at');
	}
	
    public function findOrFail($room, $id)
    {
        return $this->queryVisible($room)->findOrFail($id);
    }

	public function getLastSended($room, $before)
	{
        $messages = $this->queryVisible($room);

        $list = $before ? 
            $messages->where('id', '<', $before)->orderBy('id', 'desc')->limit(20) 
            :
            $messages->orderBy('id', 'desc')->limit(20);

        return $list->get()->reverse();
	}
}
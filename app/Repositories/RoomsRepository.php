<?php
namespace App\Repositories;

use App\Contracts\Repositories\RoomsRepositoryContract;
use App\Room;
use Illuminate\Support\Facades\Redis;

class RoomsRepository implements RoomsRepositoryContract
{
    public function all()
    {
        if (Redis::hlen('rooms') > 0) {
            $items = Redis::hvals('rooms');
        } else {
            $items = Room::all();
        }
    }

    public function getById($id)
    {
        $item = Redis::hget('rooms', "room:$id");
        if (!$item) {
            $item = Room::find($id);
        }
        return $item;
    }

    public function save($model)
    {
        //    $model->save();
        Redis::hset('rooms', 'room:'+$model->id, $model->toString());
    }
}

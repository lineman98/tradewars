<?php
namespace App\Services\CentrifugoBroadcaster;

use Illuminate\Contracts\Broadcasting\Broadcaster as BroadcasterContract;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\Broadcasters\Broadcaster as Broadcaster;

class CentrifugoBroadcaster extends Broadcaster implements BroadcasterContract
{
    public $centrifugo;

    public function __construct($centrifugo)
    {
        $this->centrifugo = $centrifugo;
    }
    /**
  * Authenticate the incoming request for a given channel.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return mixed
  */
    public function auth($request)
    {
        return [
            'channels' => array_reduce($request->channels, function ($result, $channelName) use ($request) {
                $normalizedChannelName = $this->normalizeChannelName($channelName);

                try {
                    $this->verifyUserCanAccessChannel($request, $normalizedChannelName);

                    $result[] = $this->validAuthenticationResponse($request, $channelName);
                } catch (\Exception $e) {
                    \Log::info($e->getMessage());
                }
                return $result;
            }, [])
        ];
    }

    protected function normalizeChannelName($channelName)
    {
        return str_replace('$', '', $channelName);
    }

    /**
     * Return the valid authentication response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $result
     * @return mixed
     */
    public function validAuthenticationResponse($request, $channelName)
    {
        return [
            'channel' => $channelName,
            'token' => $this->centrifugo->generatePrivateChannelToken($request->client, $channelName)
        ];
    }

    /**
     * Broadcast the given event.
     *
     * @param  array  $channels
     * @param  string  $event
     * @param  array  $payload
     * @return void
     */
    public function broadcast(array $channels, $event, array $payload = [])
    {
        $payload['event'] = $event;
        $channels = $this->formatChannels($channels);
        //dd($channels);
        $result = $this->centrifugo->broadcast($channels, $payload);

        if (property_exists($result, 'error')) {
            \Log::error('Centrifugo broadcast: ' . json_encode($result->error));
            abort($result->error->code, $result->error->message);
        }

        return true;
    }

    protected function formatChannels($channels)
    {
        return array_map(function ($channel) {
            if ($channel instanceof PrivateChannel) {
                $name = str_replace('private-', '', $channel);
                return '$' . $name;
            } elseif ($channel instanceof PresenceChannel) {
                $name = str_replace('presence-', '', $channel);
                return 'presence:'  . $name;
            }
            return (string) $channel;
        }, $channels);
    }
}

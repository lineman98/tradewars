<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('creator_id')->default(0);
            $table->enum('type', ['demo', 'real']);
            $table->integer('bet_amount');
            $table->enum('status', ['wait', 'play', 'finish', 'closed']);
            $table->string('step')->default('preparing'); // на каком этапе сейчас игра, ставка #, ожидание и т.д.
            $table->text('step_info');
            $table->integer('orders_count')->default(1);
            $table->integer('members_count')->defalut(2);
            $table->string('password')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}

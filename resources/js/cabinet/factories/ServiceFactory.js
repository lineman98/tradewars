import UsersService from '../services/UsersService';
import AuthService from '../services/AuthService';
import BroadcastService from '../services/BroadcastService';

const services = {
    users: UsersService,
    auth: AuthService,
    broadcast: BroadcastService
}

export default {
    get: name => {
        if (services[name] == undefined) throw `Factory ${name} not found`;

        return services[name]
    },
}
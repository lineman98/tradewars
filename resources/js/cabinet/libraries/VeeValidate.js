import * as VeeValidate from "vee-validate";
import * as rules from 'vee-validate/dist/rules';

const messages = {
    "alpha": "Поле может содержать только буквы",
    "alpha_dash": "Поле может содержать только буквы, цифры и дефис",
    "alpha_num": "Поле может содержать только буквы и цифры",
    "alpha_spaces": "Поле может содержать только буквы и пробелы",
    "between": "Поле должно быть между {min} и {max}",
    "confirmed": "Поле и его подтверждение не совпадают",
    "digits": "Поле должно быть числовым и точно содержать {length} цифры",
    "dimensions": "Поле должно быть {width} пикселей на {height} пикселей",
    "email": "Поле должно быть действительным электронным адресом",
    "excluded": "Поле должно быть допустимым значением",
    "ext": "Поле должно быть действительным файлом. ({args})",
    "image": "Поле должно быть изображением",
    "oneOf": "Поле должно быть допустимым значением",
    "integer": "Поле должно быть целым числом",
    "length": "Длина поля должна быть {length}",
    "max": "Поле не может быть более {length} символов",
    "max_value": "Поле должно быть {max} или менее",
    "mimes": "Поле должно иметь допустимый тип файла. ({args})",
    "min": "Не менее {length} символов",
    "min_value": "Минимальное значение - {min}",
    "numeric": "Поле должно быть числом",
    "regex": "Поле имеет ошибочный формат",
    "required": "Поле не может быть пустым",
    "required_if": "Поле обязательно для заполнения",
    "size": "Поле должно быть меньше, чем {size}KB",
    "min_amount": "Минимальная сумма - {min}$",
    "max_amount": "Максимальная сумма - {max}$",
    "between_amount": "Cумма должна быть от {min}$ до {max}$",
    "date": 'Введите корректную дату'
}


Object.keys(rules).forEach(rule => {
    VeeValidate.extend(rule, {
        ...rules[rule], // copies rule configuration
        message: messages[rule] // assign message
    });
});

VeeValidate.extend('min_amount', {
    validate(value, {min}) {
        value = Number(value)
        return value >= min
    },
    params: ['min'],
    message: messages.min_amount
})

VeeValidate.extend('max_amount', {
    validate(value, {min}) {
        value = Number(value)
        return value <= min
    },
    params: ['max'],
    message: messages.max_amount
})

VeeValidate.extend('between_amount', {
    validate(value, {min, max}) {
        value = Number(value)
        return value >= min && value <= max
    },
    params: ['min', 'max'],
    message: messages.between_amount
})

VeeValidate.extend('date', {
    validate(value) {
        let parts = value.split('.').map(item => Number(item))
        return parts[0] >= 0 && parts[0] <= 31 && parts[1] >= 1 && parts[1] <= 12 && parts[2] >= 1900 && parts[2] <= 2020
    },
    message: messages.date
})

export default VeeValidate

import {Model, Collection} from 'vue-mc'

/**
 * Task model
 */
export class Room extends Model {

    // Default attributes that define the "empty" state.
    defaults() {
        return {
			id:   null,
			creator_id: null,
			type: '',
			bet_amount: 0,
			status: 'wait',
			step: 0,
			step_info: '',
			orders_count: 0,
			members_count: 0,
			password: ''
        }
    }

    // Attribute mutations.
    mutations() {
        return {
			id:   		(val) => Number(val) || null,
			creator_id: (val) => Number(val) || null,
			type: 		String,
			bet_amount: (val) => Number(val) || null,
			status: 	(val) => Number(val) || 'wait',
			step: 		(val) => Number(val) || 60,
			step_info: 	(val) => val || '',
			orders_count: (val) => Number(val) || 0,
			members_count: (val) => Number(val) || 1,
			password: String
        }
    }

    // Attribute validation
    validation() {
        return {
            //id:   integer.and(min(1)).or(equal(null)),
            //name: string.and(required),
            //done: boolean,
        }
    }

    // Route configuration
    routes() {
        return {
            fetch: '/rooms/{id}',
            save:  '/rooms',
        }
    }
}

/**
 * Task collection
 */
export class RoomList extends Collection {

    // Model that is contained in this collection.
    model() {
        return Room;
    }

    // Default attributes
    defaults() {
        return {
            orderBy: 'id',
        }
    }

    // Route configuration
    routes() {
        return {
            fetch: '/rooms',
        }
    }

    // Number of tasks to be completed.
    get todo() {
        return this.sum('done');
    }

    // Will be `true` if all tasks have been completed.
    get done() {
        return this.todo == 0;
    }
}
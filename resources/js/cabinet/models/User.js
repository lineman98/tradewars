import {Model, Collection} from 'vue-mc'

/**
 * Task model
 */
export class User extends Model {

    // Default attributes that define the "empty" state.
    defaults() {
        return {
			id:   null,
			balance_real: 0.00,
			balance_demo: 0.00,
			email: '',
			name: ''
        }
    }

    // Attribute mutations.
    mutations() {
        return {
            id: (val) => Number(val) || null,
            balance_real: (val) => Number(val) || null,
            balance_demo: (val) => Number(val) || null,
			email: String,
			name: String
        }
    }

    // Attribute validation
    validation() {
        return {
            id:   integer.and(min(1)).or(equal(null)),
            name: string.and(required),
            done: boolean,
        }
    }

    // Route configuration
    routes() {
        return {
            fetch: '/task/{id}',
            save:  '/task',
        }
    }
}

/**
 * Task collection
 */
export class UserList extends Collection {

    // Model that is contained in this collection.
    model() {
        return User;
    }

    // Default attributes
    defaults() {
        return {
            orderBy: 'name',
        }
    }

    // Route configuration
    routes() {
        return {
            fetch: '/users',
        }
    }

    // Number of tasks to be completed.
    get todo() {
        return this.sum('done');
    }

    // Will be `true` if all tasks have been completed.
    get done() {
        return this.todo == 0;
    }
}
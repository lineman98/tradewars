import VueRouter from 'vue-router';
import Vue from 'vue'
Vue.use(VueRouter);

import Auth from './routes/auth/Auth.vue';
import Store from './store/store.js';


import Cabinet from './routes/cabinet/Cabinet.vue'
import CabinetUser from './routes/cabinet/user/User.vue'
import CabinetUserEdit from './routes/cabinet/user/edit/Edit.vue'
import CabinetUserFinances from './routes/cabinet/user/finances/Finances.vue'
import CabinetUserDeposit from './routes/cabinet/user/finances/deposit/Deposit.vue'
import CabinetUserWithdrawal from './routes/cabinet/user/finances/withdrawal/Withdrawal.vue'
import CabinetUserHistory from './routes/cabinet/user/history/History.vue'
import CabinetUserUserinfo from './routes/cabinet/user/userinfo/Userinfo.vue'
import CabinetRooms from './routes/cabinet/rooms/Rooms.vue'



/*
const Trade = () => import('./pages/trade/Trade.vue')
const Finance = () => import('./pages/finance/Finance.vue')
const Profile = () => import('./pages/profile/Profile.vue')
*/


let router = new VueRouter({
    mode: "history",
    routes: [{
            path: '/login',
            name: 'login',
            component: Auth
        },
        {
            path: '/register',
            name: 'register',
            component: Auth
        },
        {
            path: '',
            redirect: { name: 'cabinet' }
        },
        {
            path: '/home',
            redirect: { name: 'cabinet' }
        },
        {
            path: "/cabinet",
            name: "cabinet",
            component: Cabinet,
            meta: {
                authRequired: true
            },

            children: [{
                    path: '',
                    redirect: { name: 'cabinet.rooms' }
                },
                {
                    path: 'rooms',
                    name: 'cabinet.rooms',
                    component: CabinetRooms
                },
                {
                    path: 'user',
                    name: 'user',
                    component: CabinetUser,

                    children: [{
                            path: '',
                            redirect: { name: 'cabinet.user.userinfo' }
                        },
                        {
                            path: 'edit',
                            name: 'cabinet.user.edit',
                            component: CabinetUserEdit
                        },
                        {
                            path: 'finances',
                            name: 'cabinet.user.finances',
                            component: CabinetUserFinances,
                            redirect: { name: "cabinet.user.deposit" },

                            children: [{
                                    path: 'deposit',
                                    name: 'cabinet.user.deposit',
                                    component: CabinetUserDeposit,
                                    meta: {
                                        transition: 'slider-from-left'
                                    }
                                },
                                {
                                    path: 'withdrawal',
                                    name: 'cabinet.user.withdrawal',
                                    component: CabinetUserWithdrawal,
                                    meta: {
                                        transition: 'slider-from-right'
                                    }
                                }
                            ]
                        },
                        {
                            path: 'history',
                            name: 'cabinet.user.history',
                            component: CabinetUserHistory
                        },
                        {
                            path: 'userinfo',
                            name: 'cabinet.user.userinfo',
                            component: CabinetUserUserinfo
                        },
                    ]
                }
            ]
        },

    ]
})

router.beforeEach((to, from, next) => {
    to.matched.forEach(matched => {
        console.log('m', matched)
        console.log(['login', 'register'].indexOf(matched.name) !== -1, Store.getters.isLoggedIn)

        if (matched.meta.authRequired && !Store.getters.isLoggedIn) {
            next({
                name: 'login'
            })
        } else if (['login', 'register'].indexOf(matched.name) !== -1 && Store.getters.isLoggedIn) {
            next({
                name: 'cabinet.rooms'
            })
        }
    })
    next()
})

export default router
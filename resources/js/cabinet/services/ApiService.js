import axios from "axios";

const baseDomain = 'http://' + window.location.hostname //'http://tradewars.ru';
const baseURL = `${baseDomain}/api/v1/`;

export default axios.create({
    baseURL,
    headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token')
    }
})

import Api from './ApiService';

class AuthService {
    login(data) {
        return Api.post('/login', data)
    }

    register(data) {
        return Api.post('/register', data)
    }
}

export default new AuthService
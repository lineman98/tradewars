import Api from './ApiService';

class UsersRepository {
    getMe() {
        return Api.get('/users/me')
    }
}

export default new UsersRepository
let Centrifuge = require("centrifuge");


centrifuge = new Centrifuge('ws://localhost:8000/connection/websocket', {
    subscribeEndpoint: '/broadcasting/auth',
    subscribeHeaders: {
        'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').getAttribute('content')
    }
});
centrifuge.setToken(document.querySelector('meta[name=centrifugo-token]').getAttribute('content'))
centrifuge.connect();



export default centrifuge
import axios from 'axios'
import ServiceFactory from '../../factories/ServiceFactory'
const AuthService = ServiceFactory.get('auth')

export default {
    state: {
        authStatus: '',
        token: localStorage.getItem('token')
    },
    mutations: {
        auth_request(state) {
            state.authStatus = 'loading'
        },
        auth_success(state, token) {
            state.authStatus = 'success'
            state.token = token

            localStorage.setItem('token', token)
        },
        auth_error(state) {
            state.authStatus = 'error'
            localStorage.removeItem('token')
        },
        logout(state) {
            state.authStatus = ''
            state.token = ''
                //localStorage.removeItem('token')
        },
        set_user(state, user) {
            state.user = user
        }
    },
    actions: {
        login({ commit }, user) {
            commit('auth_request')
            return AuthService.login(user)
                .then(resp => {
                    let token = resp.data.token
                    let user = resp.data.user
                    axios.defaults.headers.common['Authorization'] = token
                    commit('auth_success', token)
                    commit('set_user', user)
                    return resp
                })
                .catch((err) => {
                    console.log('err', err, err.response)
                    commit('auth_error')
                    return err
                })
        },
        register({ commit }, user) {
            commit('auth_request')
            return AuthService.register(user)
                .then(resp => {
                    let token = resp.data.token
                    let user = resp.data.user
                    axios.defaults.headers.common['Authorization'] = 'Bearer: ' + token
                    commit('auth_success', token)
                    commit('set_user', user)
                    return resp
                })
                .catch(err => {
                    commit('auth_error', err)
                    return err
                })
        },
        logout({ commit }) {
            return new Promise((resolve, reject) => {
                commit('logout')
                localStorage.removeItem('token')
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        }
    },

    getters: {
        username: state => state.user.name,
        isLoggedIn: state => !!state.token,
        authStatus: state => state.authStatus,
        token: state => state.token
    }
}
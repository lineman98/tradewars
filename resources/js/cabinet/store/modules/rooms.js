import axios from 'axios'
import {Room, RoomList} from '../../models/Room'

export default {
	state: {
		rooms: new RoomList()
	},
	mutations: {
		setRooms(state, payload) {
			state.rooms = new RoomList
		}
	},
	actions: {
        syncGame({commit}) {
            return new Promise((resolve, reject) => {
                commit('logout')
                localStorage.removeItem('token')
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
		},
		fetchUser({commit}) {
			
		}
	},
	
	getters: {
    }
}
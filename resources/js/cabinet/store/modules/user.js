import axios from 'axios'
import { User } from '../../models/User'
import ServiceFactory from '../../factories/ServiceFactory'
const UsersService = ServiceFactory.get('users')

export default {
    state: {
        user: {},
        //user: new User()
    },
    mutations: {
        set_user(state, user) {
            Vue.set(state, 'user', user)
                //state.user = user
        },
        incrementBalanceReal(state, payload) {
            state.user.balance_real += payload
        },
        decrementBalanceReal(state, payload) {
            state.user.balance_real -= payload
        },
        incrementBalanceDemo(state, payload) {
            state.user.balance_demo += payload
        },
        decrementBalanceDemo(state, payload) {
            state.user.balance_demo -= payload
        },
    },
    actions: {
        fetchUser({ commit, dispatch, getters }) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + getters.token

            return UsersService.getMe()
                .then(({ data }) => commit('set_user', data))
                .catch((err) => dispatch('logout'))
        },
        updateUser({ commit, dispatch, getters }, user) {
            return axios.patch(`/api/v1/users/${getters.user.id}`, user)
                .then((resp) => commit('setUser', user))
                .catch((err) => console.log(err))
        },
        updatePassword({ commit, dispatch, getters }, password) {
            return axios.patch(`/api/v1/users/${getters.user.id}`, password)
                .then((resp) => commit('setUser', user))
        }
    },

    getters: {
        isPlaying: state => {
            //
        },
        selectedBalance: (state, getters, root) => {
            return root.user[root.balanceType + '_balance']
        },
        user: state => state.user,
        userLoaded: state => state.user.name !== undefined
    }
}
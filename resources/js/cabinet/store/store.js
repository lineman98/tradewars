import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import UserModule from "./modules/user";
import AuthModule from "./modules/auth";
import RoomsModule from "./modules/rooms";


export default new Vuex.Store({
	strict: process.env.NODE_ENV !== 'production',

    state: {
		balance_type: localStorage.getItem('balance_type') || 'demo', //real,demo
		isLocked: false
    },
    mutations: {
		balance_type(state, type) {
			state.balance_type = type
			localStorage.setItem('balance_type', type)
		},
		lock(state) {
			Vue.set(state, 'isLocked', true)
		},
		unlock(state) {
			Vue.set(state, 'isLocked', false)
		}
    },
    actions: {
		setBalanceType({commit}, type) {
			if(['real', 'demo'].indexOf(type) < 0) return false;

			commit('balance_type', type);
		}
    },

    getters: {
		balanceType: state => state.balance_type,
		isLocked: state => state.isLocked
	},
	
	modules: [
		AuthModule,
		UserModule,
		RoomsModule
	]
})

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>{{ config('app.name') }}</title>
        {{--<meta name="centrifugo-token" content="{{ $centrifugoToken }}">--}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<link href="{{ mix('css/cabinet.css') }}" rel="stylesheet" type="text/css">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
    <body>
		<div id="app">
			<router-view></router-view>
		</div>
		<script src="{{ mix('js/cabinet.js') }}"></script>
    </body>
</html>

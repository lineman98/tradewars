<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        {{--<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		--}}
		<link href="{{ mix('css/welcome.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            {{--@if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="/login">Login</a>

                        @if (Route::has('register'))
                            <a href="/register">Register</a>
                        @endif
                    @endauth
                </div>
            @endif--}}

			<main>
				<img src="/img/welcome/main-bg.png" alt="" class="main-bg">
			
				<section class="main">
					<header>
						<div class="header-left">
							<img class="logo" src="/img/welcome/logo.png" alt="">
							<span class="logo-label">Торгуй не<br>против брокера<br>а против трейдера</span>
						</div>
						<div class="header-right">
							@if (Route::has('login'))
								@auth
									<a href="{{ route('home') }}" class="btn btn-warning">Личный кабинет</a>
								@else
									@if (Route::has('register'))
									<a href="/register" class="btn btn-warning">Быстрая регистрация</a>
									@endif
									<a href="/login" class="btn btn-warning-outline">Войти</a>
								@endauth
							@endif
						</div>
					</header>
					<div class="container">
						<div class="content">
							<div class="title">
								<h1>Та самая платформа<br>
									<span class="text-warning">с доходностью до 3000%</span><br>
									о которой говорят<br>профессионалы
								</h1>
							</div>
							<span>
								Присоединяйся к новому тренду онлайн-торговли<br>
								в формате трейдерского комьюнити
							</span>
							<div>
								<a href="{{ route('register') }}" class="btn btn-warning">Присоединиться бесплатно</a>
							</div>
							<div class="list">
								<ul>
									<li>
										@component('components/check') @endcomponent
										<span>Без брокеров<br>и роботов</span>
									</li>
									<li>
										@component('components/check') @endcomponent
										<span>Напрямую<br>с живыми людьми</span>
									</li>
									<li>
										@component('components/check') @endcomponent
										<span>Призовые<br>до 100000 рублей за 1 час*</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="arrow-bottom">
						@component('components.arrow-bottom') @endcomponent
					</div>
				</section>
				<section class="overview">
					<div class="container">
						<div class="content">
							<div class="video">
								<img src="/img/welcome/laptop.png" class="laptop" alt="">
								<iframe src="https://www.youtube.com/embed/ildFSEpsLdM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
							<div class="description">
								<h2 class="title">Совершенный и<br>прозрачный алгоритм</h2>
								<p>Присоединяйся к разым торговым комнатам с другими
									трейдерами. Номинал комнат от 100 рублей до 50000
									рублей, а призовой фонд турниров составляет до 1000000
									рублей
								</p>
								<p>Выполняй задания путем правильного прогноза движения
									валютных пар и побеждай, забирай призовой фонд и 
									реализовывай мечты за счет своих знаний о торговле
								</p>
								<a href="{{ route('login') }}" class="btn btn-warning">Зарегистрироваться</a>
							</div>
						</div>

						<div class="banner">
							<div class="titles">
								<h1 class="title">Торгуешь недавно?</h1>
								<h4 class="subtitle">Попробуй бесплатно на демо счете</h4>
							</div>
							<div class="description">
								<p>
									Здесь полностью отсутствуют
									риски, т.к. счет и деньги - 
									виртуальные, а процесс 1 в 1
									идентичен реальной игре
								</p>
								<a href="{{ route('register') }}" class="btn btn-warning">Попробовать демо</a>
							</div>
						</div>
					</div>
				</section>
				<section class="advantages">
					<div class="container">
						<div class="content">
							<h1 class="title">
								Испытай все плюсы <span class="text-warning">новой</span> <br>платформы
							</h1>
							<div class="list">
								<ul>
									<li class="item">
										<span class="title">Live-торговля</span>
										<p>
											Все прогнозы совершаются между реальными людьми, а не брокером, который изначально заинтересован в поражении пользователя.
											Побеждают только знание и удача, а сервис помогает создавать для этого комфортные условия
										</p>
									</li>
									<li class="item">
										<span class="title">Комьюнити Трейдеров</span>
										<p>
											Узнавайте своих соперников, делитесь опытом, общайтесь в общем и приватных чатах, добавляйте друзей, заводите новые полезные и интересные контакты 
										</p>
									</li>
									<li class="item">
										<span class="title">Топовые Технологии</span>
										<p>
											Используем лучших поставщиков котировок, функционал сайта проходит регулярный апгрейд, 
											новые инструменты для удобной торговли выходят ежемесячно 
										</p>
									</li>
									<li class="item">
										<span class="title">Надежно И Безопасно</span>
										<p>
											Проект имеет все необходимые официальные лицензии и прошел проверки безопасности. Легкое пополнение, быстрый вывод и защита ваших активов передовыми технологиями платежных систем - наш главный приоритет
										</p>
									</li>
								</ul>
							</div>
							<div class="btn-register">
								<a href="{{ route('register') }}" class="btn btn-warning-outline">Начать зарабатывать</a>
							</div>
						</div>
					</div>
				</section>
				<section class="principles">
					<div class="container">
						<div class="content">
							<img src="/img/welcome/man.png" class="man">
							<h1 class="title">
								Чего здесь <br><span class="strong">нет и не будет</span>
							</h1>
							<div class="list">
								<ul>
									<li class="item">
										<div class="icon">
											<img src="/img/welcome/magic-trick.png" alt="magic-trick">
										</div>
										<div class="title">
											Уловок и хитростей<br>администрации проекта
										</div>
										<p>
											Ведь проект не заинтересован в поражении пользователя
										</p>
									</li>
									<li class="item">
										<div class="icon">
											<img src="/img/welcome/stocks.png" alt="stocks">
										</div>
										<div class="title">
											Преднамеренного искажения графика
										</div>
										<p>
											Пользователи часто сталкиваются с подобным "грязным" приемом. Движение графика в проекте основано исключительно на текущих рыночных котировках
										</p>
									</li>
									<li class="item">
										<div class="icon">
											<img src="/img/welcome/sale.png" alt="sale">
										</div>
										<div class="title">
											Спецпредложений и<br>акций с подвохом
										</div>
										<p>
											Многие трейдеры попадали в ситуацию, когда платформа навязывает акцию из за которой в дальнейшем вывод средств практически невозможен или невыгоден 
										</p>
									</li>
									<li class="item">
										<div class="icon">
											<img src="/img/welcome/decrease.png" alt="decrease">
										</div>
										<div class="title">
											Ботов и алгоритмов направленых на слив игрока
										</div>
										<p>
											Этот проект для людей и всё в нем решают реальные живые люди - участники 
										</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section class="calculate">
					<img src="/img/welcome/cup-big.png" alt="" class="cup-big">
					<div class="container">
						<div class="content">
							<h1 class="title">
								Платформа, которая платит<br>
								за победу больше!
							</h1>
							<p>Узнайте сколько приносит участие в турнирах и торговых комнатах</p>
							<div class="toggler">
								<label for="">Первый взнос:</label>
							</div>
							<div class="prises">
								<div>
									<div class="label">
										При за победу в торговой комнате:
									</div>
									<div class="value">
										<img src="/img/welcome/cup.png" alt="" class="icon"> 12 000 рублей
									</div>
								</div>
								<div>
									<div class="label">
										Приз за победу в турнире:
									</div>
									<div class="value">
										<img src="/img/welcome/cup.png" alt="" class="icon"> 72 000 рублей
									</div>
								</div>
							</div>
							<div class="btn-register">
								<a href="{{ route('register') }}" class="btn btn-warning">Принять участие</a>
							</div>
						</div>
					</div>
				</section>
				<section class="versus">
					<div class="container">
						<div class="content">
							<h1 class="title">
								Турниры с призовыми <br><span class="text-warning">до 100 000 рублей</span>
							</h1>
							<p class="description">
								Хотите вывозить по крупному? Тогда наши турниры вас обрадуют.
								Присоединяйтесь к регулярным турнирам, которые проходят по расписанию.
								Увидев размер призовых, вы точно скажете “Вау!”
							</p>
							<a href="{{ route('register') }}" class="text-warning btn-register">Принять участие</a>
							<img src="/img/welcome/versus-bg.png" class="versus-bg" alt="VS"> 
							<div class="versus-labels">
								<span class="left">
									Бесплатный<br>турнир
								</span>
								<div class="right">
									Учавствуй бесплатно,<br>выигрывай и забирай<br>
									<div class="strong">призовые 75 000 рублей<br>без риска</div>
								</div>
							</div>
							<div class="versus-bottom">
								<p>Осталось <span class="text-warning">9</span> мест</p>
								<a href="{{ route('register') }}" class="btn btn-warning">Вступить бесплатно</a>
							</div>
							<div class="list">
								<ul>
									<li class="item">
										<div class="icon">
											<img src="/img/welcome/tablet.png" alt="" title="">
										</div>
										<span class="title">Удобно С Любого Устройства</span>
										<p>
											Платформа полностью адаптирована<br>
											под все популярные<br>
											виды устройств
										</p>
									</li>
									<li class="item">
										<div class="icon">
											<img src="/img/welcome/speak.png" alt="" title="">
										</div>
										<span class="title">Live-общение</span>
										<p>
											Все комнаты и интерфейс имеют<br>
											чаты для трейдеров, знакомьтесь,<br>
											общайтесь, объединяйтесь.
										</p>
									</li>
									<li class="item">
										<div class="icon">
											<img src="/img/welcome/award.png" alt="" title="">
										</div>
										<span class="title">Награды, Ранги, Дуэли</span>
										<p>
											Получайте достижения,
											зарабатывайте статусы и авторитет
											среди сообщества трейдеров,
											соревнуйтесь 1 на 1
										</p>
									</li>
									<li class="item">
										<div class="icon">
											<img src="/img/welcome/travel.png" alt="" title="">
										</div>
										<span class="title">Трейдеры Из Разных Стран</span>
										<p>
											Платформа многоязычна,<br>
											перенимай опыт и общайся с 
											зарубежными игроками
										</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section class="from-traders">
					<div class="container">
						<div class="content">
							<h1 class="title">
								От трейдеров<br>для трейдеров
							</h1>
							<p class="slogan">Обучайся, развивайся, богатей</p>
							<p>
								Traders Club - одно из немногих сообществ трейдеров в
								интернете, где тебе рады и никто не заинтересован в твоем
								“сливе”, а заработать можно даже без денег
							</p>
							<div class="btn-register">
								<a href="{{ route('register') }}" class="btn btn-warning">Зарегистрироваться бесплатно</a>
							</div>
						</div>
						<img src="/img/welcome/moneycase.png" class="moneycase" alt="" title="">
						<div class="list">
							<ul>
								<li class="item">
									<span class="icon">
										@component('components.check') @endcomponent
									</span>
									<span>
										Десятки бесплатных<br>
										демо-комнат для обучения
									</span>
								</li>
								<li class="item">
									<span class="icon">
										@component('components.check') @endcomponent
									</span>
									<span>
										Общение с более опытными<br>
										трейдерами, прямые трансляции торговли
									</span>
								</li>
								<li class="item">
									<span class="icon">
										@component('components.check') @endcomponent
									</span>
									<span>
										Бесплатные турниры<br> реальными призовыми
									</span>
								</li>
							</ul>
						</div>
					</div>
				</section>
				<section class="partners">
					<div class="container">
						<div class="slider">
							<img src="/img/welcome/slider-placeholder.png" alt="">
							<img src="/img/welcome/slider-placeholder.png" alt="">
							<img src="/img/welcome/slider-placeholder.png" alt="">
							<img src="/img/welcome/slider-placeholder.png" alt="">
							<img src="/img/welcome/slider-placeholder.png" alt="">
						</div>
					</div>
				</section>
				<section class="bars">
					<div class="container">
						<div class="content">
							<h1 class="title">
								Здесь будут все
							</h1>
							<div class="divider"></div>
							<p>
								Будь одним из первых в крупнейшем источнике денег и полезного опыта, которые
								когда-либо умещались в одну платформу
							</p>
							<div class="btn-register">
								<a href="{{ route('register') }}" class="btn btn-warning">Начать зарабатывать</a>
							</div>
						</div>
					</div>
				</section>
				<footer class="footer">
					<div class="container">
						<div class="content">
							<div class="inline-icons">
								<div class="list left">
									<ul>
										<li class="item">
											<img class="icon logo" src="/img/welcome/logo.png" alt="">
										</li>
										<li class="item">
											<img class="icon age" src="/img/welcome/age.png" alt="">
										</li>
									</ul>	
								</div>
								<div class="list right">
									<ul>
										<li class="item">
											Мы в соц. сетях: 
										</li>
										<li class="item social">
											<a href="">
												<img class="icon" src="/img/welcome/youtube.png" alt="">
											</a>
										</li>
										<li class="item social">
											<a href="">
												<img class="icon" src="/img/welcome/instagram.png" alt="">
											</a>
										</li>
										<li class="item social">
											<a href="">
												<img class="icon" src="/img/welcome/vk.png" alt="">
											</a>
										</li>
									</ul>	
								</div>
							</div>
							<div class="info">
								<p>
									Traders Club — Зарегистрированная компания в Белизе, что гарантирует клиентам компании качество услуг, прозрачность взаимоотношений, защиту нейтральной и независимой организации по урегулированию споров на основании Гаагской конвенции 1961 года об Апостиле. и IBC Act.  
								</p>
								<p>
									Финансовые операции, предлагаемые данным сайтом, могут обладать повышенным уровнем риска. Приобретая предлагаемые данным сайтом финансовые услуги и инструменты, вы можете понести серьезные финансовые потери или полностью утратить средства, находящиеся на вашем гарантийно-торговом счету. Пожалуйста, оцените все финансовые риски и посоветуйтесь с независимым финансовым консультантом
								</p>
							</div>
							<h3 class="info-extend">
								<a href="">Читать полостью...</a>
							</h3>
							<div class="navbar">
								<ul>
									<li class="item"><a href="">Контакты</a></li>
									<li class="item"><a href="">Политика платежей</a></li>
									<li class="item"><a href="">Договор публичной оферты</a></li>
									<li class="item"><a href="">Политика AML и KYC</a></li>	
									<li class="item"><a href="">Политика конфидениальности</a></li>
									<li class="item"><a href="">Предупреждение о риске</a></li>
									<li class="item"><a href=""><b>Trade-site 2020</b></a></li>
								</ul>
							</div>
						</div>
					</div>
				</footer>
			</main>
        </div>
    </body>
	<script src="{{ mix('js/public.js') }}"></script>
</html>

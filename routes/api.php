<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->prefix('v1')->group(function() {
	Route::get('/users/me', 'API\UsersController@me');
});


Route::group(['prefix' => '/v1'], function() {
	Route::any('/login', 'API\AuthController@login');
	Route::any('/register', 'API\AuthController@register');

	Route::group(['prefix' => 'chat'], function() {
		Route::get('/{room}', 'API\ChatController@history');
		Route::get('/{room}/{before}', 'API\ChatController@history');
		Route::post('/{room}', 'API\ChatController@post');
		Route::patch('/{room}/{message}', 'API\ChatController@edit');
		Route::delete('/{room}/{message}', 'API\ChatController@delete');
	});
});
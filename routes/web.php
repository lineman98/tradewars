<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/cabinet', 'Profile\HomeController@index')->name('home');
Route::get('/login', 'Profile\HomeController@index')->name('login');
Route::get('/register', 'Profile\HomeController@index')->name('register');

Route::fallback('Profile\HomeController@index');


Route::get('c', function () {
    broadcast(new \App\Events\UserUpdated(\Request::user()))->toOthers();
});

Route::get('/test', 'TestController@index');

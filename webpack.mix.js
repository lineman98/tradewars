const mix = require('laravel-mix');
//require('laravel-mix-bundle-analyzer');

//if (!mix.inProduction()) {
//    mix.bundleAnalyzer();
//}
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
/*
mix.webpackConfig({
	resolve: {
		devServer: {
			disableHostCheck: true
		}
	}
})*/

mix.js('resources/js/cabinet/app.js', 'public/js/cabinet.js').version()
    .js('resources/js/public/app.js', 'public/js/public.js').version()
    .sass('resources/sass/public/welcome.scss', 'public/css/welcome.css').version().options({ processCssUrls: false })
    .sass('resources/sass/cabinet/cabinet.scss', 'public/css/cabinet.css').version().options({ processCssUrls: false })
    .webpackConfig({
        resolve: {
            alias: {
                '@vars': path.resolve('resources/sass/cabinet/_variables.scss')
            }
        }
    })